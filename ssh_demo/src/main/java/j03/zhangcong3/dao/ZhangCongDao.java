package j03.zhangcong3.dao;

import j03.zhangcong3.entity.ZhangCongEntity;
import org.hibernate.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.orm.hibernate4.HibernateTemplate;
import org.springframework.orm.hibernate4.support.HibernateDaoSupport;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class ZhangCongDao  extends HibernateDaoSupport {


    public List<ZhangCongEntity> query(){
        Query query=getSessionFactory().openSession().createQuery("from ZhangCongEntity");
        return  query.list();
    }
}
