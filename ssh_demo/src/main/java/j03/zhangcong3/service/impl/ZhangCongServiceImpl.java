package j03.zhangcong3.service.impl;

import j03.zhangcong3.dao.ZhangCongDao;
import j03.zhangcong3.entity.ZhangCongEntity;
import j03.zhangcong3.service.ZhangCongService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

@Service("zCservice")
public class ZhangCongServiceImpl implements ZhangCongService {
    @Autowired
    ZhangCongDao zhangCongDao;

    @Override
    public List<ZhangCongEntity> hello() {
        return zhangCongDao.query();
    }
}
