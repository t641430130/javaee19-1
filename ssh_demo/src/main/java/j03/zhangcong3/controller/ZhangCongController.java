package j03.zhangcong3.controller;

import j03.zhangcong3.entity.ZhangCongEntity;
import j03.zhangcong3.service.ZhangCongService;

import javax.annotation.Resource;
import java.util.List;

public class ZhangCongController {
    List<ZhangCongEntity> list = null;
    @Resource
    ZhangCongService zCservice;


    public String hello(){
        list =zCservice.hello();
        return "hello";
    }

    public List<ZhangCongEntity> getList() {
        return list;
    }

    public void setList(List<ZhangCongEntity> list) {
        this.list = list;
    }
}
