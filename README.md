# javaee19-1

#### 介绍

javaee19-1仓库中的项目是为了19级j03、j04、j05、j06四个班级使用的练习团队协同开发的项目。是为了帮助学生提前适应企业开发模式而创建的。

#### 软件架构

本仓库中主要包含两个模块，两个模块分别使用的是`SSH框架`(Struts,Spring,Hibernate)和`SSM框架`(Spring,SpringMVC,MyBatis)。

在两个模块中，都有着类似的包结构。在`java`和`webapp`包下是不同班级的`班级包`，每位同学需要在`班级包`下创建自己的`个人包`，命名为小写拼音全拼，在`个人包`下创建自己需要的`项目包`结构，例如在自己的包下创建controller、dao、pojo、service、serviceImpl等包。

#### 安装和使用

根据仓库地址 https://gitee.com/zhangcong_v/javaee19-1.git 使用IDEA开发工具Clone即可自动下载项目。
下载完成后需要进行的操作：

1. 配置Maven
2. 更新Maven仓库，下载项目需要的jar包
3. 配置Tomcat

#### 参与和上传

1. 创建一个自己的副本，点击`Fork`，将仓库 Fork 到自己的账号中（会发现自己的账号下会存在一个`javaee19-1`的仓库）
2. 在IDEA中`Clone`下载代码，将 https://gitee.com/zhangcong_v/javaee19-1.git 仓库中的代码Clone下载到本地
3. 对本地代码进行修改，修改时需要注意**包结构**
4. 我们需要先将代码上传到**自己的**仓库中，在`Git Remotes`中添加自己仓库的地址
5. `Update Project`需要获取最新代码，点击`Update Project`进行获取最新的代码，注意`Update Project`操作后**可以**立刻进行`Push`操作
6. `Push`上传本地代码，将本地自己写的代码`Push`到**自己的**Gitee仓库中，注意`Push`之前**必须**进行`Update Project`操作
7. 在`Gitee`平台上新建`Pull Request`，填写本次修改的相关信息，以便项目经理进行审核
8. 等待项目经理审核
9. 由项目经理合并，完成代码上传