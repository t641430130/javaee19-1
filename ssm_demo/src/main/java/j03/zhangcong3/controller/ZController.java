package j03.zhangcong3.controller;

import com.alibaba.fastjson.JSONArray;
import j03.zhangcong3.pojo.ZCPojo;
import j03.zhangcong3.service.ZCservice;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.annotation.Resource;
import java.util.List;

@Controller
public class ZController {
     @Resource
     ZCservice zCservice;

     @RequestMapping("/hello")
     @ResponseBody
     public String hello(){
        List<ZCPojo> list = zCservice.hello();
        return  JSONArray.toJSONString(list);
     }

}
