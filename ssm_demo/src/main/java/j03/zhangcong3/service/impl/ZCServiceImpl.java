package j03.zhangcong3.service.impl;

import j03.zhangcong3.dao.ZhangCongDao;
import j03.zhangcong3.pojo.ZCPojo;
import j03.zhangcong3.service.ZCservice;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service("zCservice")
public class ZCServiceImpl implements ZCservice {
    @Autowired
    ZhangCongDao zhangCongDao;

    public List<ZCPojo> hello() {
        return zhangCongDao.hello();
    }
}
